create table icelog (
  lognum int primary key auto_increment,
  logdate datetime,
  logmachine varchar(255),
  logpos int,
  customer varchar(255),
  liveflag char(1),
  hostname varchar(255),
  start int,
  mountpoint varchar(255),
  bytes int,
  useragent varchar(255),
  seconds int
);

